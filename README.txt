Document Version

This project is designed to assist site managers who are serving documents (PDFs, Word Documents) via their websites and they only want to serve the latest version of this document.

It is helpful for websites that provide policies, terms and conditions which need constant updating.

This project provides:

    Document Versioning
    Document Scheduling

It provides a configurable download path for your documents:

{base_path}/{document_bundle}/{document_name}

Base Path is configurable via the module settings page (/admin/structure/document_version/settings)
Document Bundles are managed in the document bundle page (/admin/structure/document_entity_type)
Documents are managed via /admin/structure/document_entity
